/*
 * Copyright 2015 the original author or authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.couchbase.devex;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.couchbase.repository.config.EnableCouchbaseRepositories;

import java.util.Date;

/**
 * Entry point of the application. Injecting Couchbase configuration through the
 * {@link com.couchbase.devex.Database} object. The
 * {@link com.couchbase.devex.ImportJsonToCouchbase} object is writing any
 * Couchbase {@link com.couchbase.client.java.document.Document} instance to the
 * configured Couchbase Server.
 * 
 * Importers are injected depending on the configuration properties. All
 * importers must implement the @{link com.couchbase.devex.ImporterConfig}
 * interface. They must return an Observable of
 * {@link com.couchbase.client.java.document.Document}.
 * 
 * @author ldoguin
 */

@SpringBootApplication
@EnableCouchbaseRepositories(basePackages = {"com.couchbase.devex"})
public class CouchbaseImporterApplication implements CommandLineRunner {

    @Autowired
    private Gson gson;


    @Bean
    public Gson gson() {
        return new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ssz").create();
    }
    @Autowired
    private EntityStatusRepository entityStatusRepository;




    public static void main(final String[] args) {
		SpringApplication.run(CouchbaseImporterApplication.class, args);
	}

    @Autowired
	private ImporterConfig selectedConfig;



    @Override
    public void run(final String... args) throws Exception {

        selectedConfig.startImport().flatMap(document -> {
            System.out.println(document.toJson());

            final String id = document.getString("_id");
            final String type = document.getString("type");
            final String typeId = document.getString("typeId");
            final String system = document.getString("system");
            final String uuid = document.getString("uuid");

            final Date expire = document.getDate("expire");
            final Date sentDate = document.getDate("sentDate");
            final Date lastUpdate = document.getDate("lastUpdate");

            final String status = document.getString("status");
            final Boolean mkp = document.getBoolean("mkp");

            final EntityStatus entityStatus = new EntityStatus(id, type, typeId, system, uuid, expire, sentDate, lastUpdate, status, mkp);
//            final EntityStatus entityStatus = gson.fromJson(document, EntityStatus.class);
            System.out.println(entityStatus);
            entityStatusRepository.save(entityStatus);
            return null;
        }).toBlocking().last();
    }
}
