package com.couchbase.devex;

import com.couchbase.client.java.repository.annotation.Id;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.annotation.Persistent;
import org.springframework.data.couchbase.core.mapping.Document;

import java.util.Date;

@Document
public class EntityStatus {

    @Id
    private String id;
    private String type;
    private String typeId;
    private String system;
    private String uuid;
    private String status;

    @JsonIgnore
    private Boolean mkp;

    @Persistent
    private Date expire;

    @Persistent
    private Date sentDate;

    @Persistent
    @JsonIgnore
    private Date lastUpdate = null;

    @PersistenceConstructor
    public EntityStatus(final String id, final String type, final String typeId, final String system, final String uuid,
                        final Date expire, final Date sentDate, final Date lastUpdate, final String status, final Boolean mkp) {
        this.id = id;
        this.mkp = mkp;
        this.type = type;
        this.typeId = typeId;
        this.system = system;
        this.uuid = uuid;
        this.expire = expire;
        this.sentDate = sentDate;
        this.status = status;
        this.lastUpdate = lastUpdate;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(final String uuid) {
        this.uuid = uuid;
    }

    public Date getExpire() {
        return expire;
    }

    public void setExpire(final Date expire) {
        this.expire = expire;
    }

    public Date getSentDate() {
        return sentDate;
    }

    public void setSentDate(final Date sentDate) {
        this.sentDate = sentDate;
    }

    public String getType() {
        return type;
    }

    public String getTypeId() {
        return typeId;
    }

    public String getSystem() {
        return system;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(final Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getStatus() {
        return status;
    }

    public Boolean isMkp() {

        if(this.mkp == null){
            this.mkp = false;
        }

        return mkp;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((expire == null) ? 0 : expire.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((lastUpdate == null) ? 0 : lastUpdate.hashCode());
        result = prime * result + (mkp ? 1231 : 1237);
        result = prime * result + ((sentDate == null) ? 0 : sentDate.hashCode());
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        result = prime * result + ((system == null) ? 0 : system.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        result = prime * result + ((typeId == null) ? 0 : typeId.hashCode());
        result = prime * result + ((uuid == null) ? 0 : uuid.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final EntityStatus other = (EntityStatus) obj;
        if (expire == null) {
            if (other.expire != null)
                return false;
        } else if (!expire.equals(other.expire))
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (lastUpdate == null) {
            if (other.lastUpdate != null)
                return false;
        } else if (!lastUpdate.equals(other.lastUpdate))
            return false;
        if (mkp != other.mkp)
            return false;
        if (sentDate == null) {
            if (other.sentDate != null)
                return false;
        } else if (!sentDate.equals(other.sentDate))
            return false;
        if (status == null) {
            if (other.status != null)
                return false;
        } else if (!status.equals(other.status))
            return false;
        if (system == null) {
            if (other.system != null)
                return false;
        } else if (!system.equals(other.system))
            return false;
        if (type == null) {
            if (other.type != null)
                return false;
        } else if (!type.equals(other.type))
            return false;
        if (typeId == null) {
            if (other.typeId != null)
                return false;
        } else if (!typeId.equals(other.typeId))
            return false;
        if (uuid == null) {
            if (other.uuid != null)
                return false;
        } else if (!uuid.equals(other.uuid))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "EntityStatus [id=" + id + ", type=" + type + ", typeId=" + typeId + ", system=" + system + ", uuid="
                + uuid + ", status=" + status + ", mkp=" + mkp + ", expire=" + expire + ", sentDate=" + sentDate
                + ", lastUpdate=" + lastUpdate + "]";
    }

}