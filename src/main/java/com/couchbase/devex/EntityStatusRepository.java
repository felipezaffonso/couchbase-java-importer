package com.couchbase.devex;

import org.springframework.data.couchbase.repository.CouchbaseRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface EntityStatusRepository extends CouchbaseRepository<EntityStatus, String> {

    EntityStatus findById(final String id);

}
