package com.couchbase.devex;

import com.google.gson.annotations.SerializedName;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by felipe.affonso on 26/01/17.
 */
public class GsonDate {

    private static SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");

    @SerializedName("$date")
    public Long date;

    public GsonDate(final Long date) {
        this.date = date;
    }

    public Date getDate() {
        return new Date(date);
    }

    public void setDate(final Date date) {
        this.date = date.getTime();
    }

    @Override
    public String toString() {
        return "date= " + date != null ? sdf.format(new Date(date)) : null;
    }
}
